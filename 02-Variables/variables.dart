void main() {
  // 1. Membuat variabel dengan tipe data
  String nama = "Budi Setiawan";
  int umur = 27;
  double tinggi = 173.5;
  bool jomblo = true;

  // 2. Membuat variabel dengan var (dinamis)
  var alamat = "Bali, Indonesia";

  // Menampilkan isi variabel
  print("Nama saya $nama. Umur saya $umur tahun. Tinggi saya $tinggi cm.");
  print("Jomblo? $jomblo");
  print("Alamat saya $alamat.");
}
