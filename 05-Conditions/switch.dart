import 'dart:io';

void main() {
  stdout.write("Masukkan peringkat: ");
  int rank = int.parse(stdin.readLineSync());

  // switch case
  switch (rank) {
    case 1:
      print("Selamat, Anda mendapat sebuah mobil!");
      break;
    case 2:
      print("Selamat, Anda mendapat sebuah sepeda motor!");
      break;
    case 3:
      print("Selamat, Anda mendapat sebuah sepeda!");
      break;
    case 4:
    case 5:
      print("Selamat, Anda mendapat uang Rp 500.000,00!");
      break;
    default:
      print("Maaf, Anda tidak mendapat hadiah.");
  }
}
