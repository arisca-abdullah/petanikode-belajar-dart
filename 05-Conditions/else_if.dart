import 'dart:io';

void main() {
  stdout.write("Masukkan nilai: ");
  double nilai = double.parse(stdin.readLineSync());

  // if, else if, else
  if (nilai > 90) {
    print("Kamu di kelas A!");
  } else if (nilai > 80) {
    print("Kamu di kelas B!");
  } else if (nilai > 70) {
    print("Kamu di kelas C!");
  } else {
    print("Silahkan kerjakan ulang tesnya!");
  }
}
