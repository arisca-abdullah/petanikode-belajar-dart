import 'dart:io'; // Mengimpor library input & output

void main() {
  // Menampilkan tulisan tanpa menambah baris baru di akhir
  stdout.write("Siapa kamu? ");

  // Menyimpan inputan ke variabel
  var nama = stdin.readLineSync();

  print("Nama kamu adalah $nama");
}
