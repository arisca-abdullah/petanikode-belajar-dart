import 'dart:io';

// Kelas
class Orang {
  // Properti
  String _nama;
  int _umur;
  double _tinggi;
  bool _menikah;
  List<String> _hobi = [];

  // Constructor
  Orang(String nama, int umur, double tinggi, bool menikah) {
    this._nama = nama;
    this._umur = umur;
    this._tinggi = tinggi;
    this._menikah = menikah;
  }

  // Methods
  void cetakDataDiri() {
    print("======= Data Diri =======");
    print("Nama     : ${this._nama}");
    print("Umur     : ${this._umur}");
    print("Tinggi   : ${this._tinggi}");
    print("Menikah  : ${this._menikah}");
  }

  void tambahHobi(String hobi) {
    this._hobi.add(hobi);
  }

  void tampilkanHobi() {
    print("${this._nama} memiliki hobi: ");

    int i = 1;
    for (var hobi in this._hobi) {
      print("${i++}. $hobi");
    }
  }
}

void main() {
  stdout.write("Siapa nama Anda: ");
  String nama = stdin.readLineSync();

  stdout.write("Berapa umur Anda: ");
  int umur = int.parse(stdin.readLineSync());

  stdout.write("Berapa tinggi Anda: ");
  double tinggi = double.parse(stdin.readLineSync());

  stdout.write("Sudah menikah? (y/t) ");
  String konfirmasi = stdin.readLineSync();
  bool menikah = konfirmasi.toLowerCase() == "y" ? true : false;

  // Membuat objek
  Orang budi = new Orang(nama, umur, tinggi, menikah);

  bool tambah = true;
  while (tambah) {
    stdout.write("Apa hobi Anda? ");

    String hobi = stdin.readLineSync();
    budi.tambahHobi(hobi); // Menambah hobi

    stdout.write("Ada lagi? (y/t) ");

    String konfirmasi = stdin.readLineSync();
    tambah = konfirmasi.toLowerCase() == "y" ? true : false;
  }

  // Memanggil method dari objek budi
  print("\n");
  budi.cetakDataDiri();
  print("=========================");
  budi.tampilkanHobi();
}
