import 'dart:io';

void main() {
  stdout.write("Masukkan angka: ");

  // Pengisian
  double angka = double.parse(stdin.readLineSync());
  print("Angka mula-mula: $angka");

  // Penjumlahan dan pengisian
  stdout.write("$angka ditambah 5.0 menjadi ");
  angka += 5;
  print(angka);

  // Pengurangan dan pengisian
  stdout.write("$angka dikurangi 2.0 menjadi ");
  angka -= 2;
  print(angka);

  // Perkalian dan pengisian
  stdout.write("$angka dikali 5.0 menjadi ");
  angka *= 5;
  print(angka);

  // Pembagian dan pengisian
  stdout.write("$angka dibagi 4.0 menjadi ");
  angka /= 4;
  print(angka);

  // Sisa bagi dan pengisian
  stdout.write("Sisa bagi $angka dengan 8.0 adalah ");
  angka %= 8;
  print(angka);
}
