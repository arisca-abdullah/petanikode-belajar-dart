import 'dart:io';

void main() {
  stdout.write("Masukkan angka pertama: ");
  double angka1 = double.parse(stdin.readLineSync());
  stdout.write("Masukkan angka kedua: ");
  double angka2 = double.parse(stdin.readLineSync());

  bool hasil;

  // Sama dengan
  hasil = angka1 == angka2;
  print("$angka1 == $angka2 : $hasil");

  // Tidak sama dengan
  hasil = angka1 != angka2;
  print("$angka1 != $angka2 : $hasil");

  // Lebih besar dari
  hasil = angka1 > angka2;
  print("$angka1 > $angka2 : $hasil");

  // Lebih kecil dari
  hasil = angka1 < angka2;
  print("$angka1 < $angka2 : $hasil");

  // Lebih besar atau sama dengan
  hasil = angka1 >= angka2;
  print("$angka1 >= $angka2 : $hasil");

  // Lebih kecil atau sama dengan
  hasil = angka1 <= angka2;
  print("$angka1 <= $angka2 : $hasil");
}
