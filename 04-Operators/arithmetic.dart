import 'dart:io';

void main() {
  stdout.write("Masukkan angka pertama: ");
  double angka1 = double.parse(stdin.readLineSync());
  stdout.write("Masukkan angka kedua: ");
  double angka2 = double.parse(stdin.readLineSync());

  var hasil;

  // "+" Penjumlahan
  hasil = angka1 + angka2;
  print("$angka1 + $angka2 = $hasil");

  // "-" Pengurangan
  hasil = angka1 - angka2;
  print("$angka1 - $angka2 = $hasil");

  // "*" Perkalian
  hasil = angka1 * angka2;
  print("$angka1 * $angka2 = $hasil");

  // "/" Pembagian
  hasil = angka1 / angka2;
  print("$angka1 / $angka2 = $hasil");

  // "~/" Pembagian (dibulatkan ke bawah)
  hasil = angka1 ~/ angka2;
  print("$angka1 ~/ $angka2 = $hasil");

  // "%" Sisa bagi
  hasil = angka1 % angka2;
  print("$angka1 % $angka2 = $hasil");
}
