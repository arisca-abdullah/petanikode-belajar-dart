void main() {
  bool a = true;
  bool b = false;

  bool hasil;

  // "&&" AND
  print("======== AND ========");
  hasil = a && a;
  print("$a  AND $a  = $hasil");
  hasil = a && b;
  print("$a  AND $b = $hasil");
  hasil = b && b;
  print("$b AND $b = $hasil");

  // "||" OR
  print("========= OR =========");
  hasil = a || a;
  print("$a  OR $a  = $hasil");
  hasil = a || b;
  print("$a  OR $b = $hasil");
  hasil = b || b;
  print("$b OR $b = $hasil");

  // "!" NOT
  print("====== NOT ======");
  hasil = !a;
  print("NOT $a  = $hasil");
  hasil = !b;
  print("NOT $b = $hasil");
}
