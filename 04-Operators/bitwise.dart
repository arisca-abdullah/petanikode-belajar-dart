void main() {
  int angka1 = 13; // 00001101
  int angka2 = 6; // 00000110

  int hasil;

  // AND
  hasil = angka1 & angka2; // 00000100 (4)
  print("$angka1 & $angka2 = $hasil");
  // OR
  hasil = angka1 | angka2; // 00001111 (15)
  print("$angka1 | $angka2 = $hasil");
  // XOR
  hasil = angka1 ^ angka2; // 00001011 (11)
  print("$angka1 ^ $angka2 = $hasil");
  // NOT
  hasil = ~angka2; // 11111001 (-7)
  print("~$angka2 = $hasil");

  // Left shift
  hasil = angka1 << 1; // 00011010 (26)
  print("$angka1 << 1 = $hasil");
  // Right shift
  hasil = angka2 >> 1; // 00000011 (3)
  print("$angka2 >> 1 = $hasil");
}
