import 'dart:io';

void main() {
  // Membuat list
  List<String> favs = new List(3);

  print("Sebutkan 3 hal yang kamu sukai!");

  for (int i = 0; i < 3; i++) {
    stdout.write("${i + 1}. ");
    favs[i] = stdin.readLineSync(); // Menambah item list
  }

  // Menampilkan isi list
  print("\nBerikut 3 hal yang kamu sukai:");

  int i = 1;
  for (String fav in favs) {
    print("${i++}. $fav");
  }
}
