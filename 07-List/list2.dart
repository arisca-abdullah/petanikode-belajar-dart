import 'dart:io';

void main() {
  // Membuat list
  List<String> mata_pelajaran = [];

  bool tambah = true;
  while (tambah) {
    stdout.write("Tambah Mata Pelajaran: ");

    String mapel_baru = stdin.readLineSync();
    mata_pelajaran.add(mapel_baru); // Menambah item list

    stdout.write("Tambah lagi? (y/t) ");

    String konfirmasi = stdin.readLineSync();
    tambah = konfirmasi.toLowerCase() == "y" ? true : false;
  }

  print("\nDaftar Mata Pelajaran: ");

  int i = 1;
  for (String mapel in mata_pelajaran) {
    print("${i++}. $mapel");
  }
  print("Total: ${mata_pelajaran.length} pelajaran.");
}
