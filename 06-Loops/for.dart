import 'dart:io';

void main() {
  stdout.write("Jumlah perulangan: ");
  int n = int.parse(stdin.readLineSync());

  // for
  for (int i = 0; i < n; i++) {
    print("Ini perulangan ke-${i + 1}");
  }
}
