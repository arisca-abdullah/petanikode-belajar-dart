void main() {
  List<String> buah_buahan = ["Apel", "Jeruk", "Jambu", "Sirsak"];

  print("Daftar buah-buahan: ");

  int i = 1;
  // for in
  for (String buah in buah_buahan) {
    print("${i++}. $buah");
  }
}
