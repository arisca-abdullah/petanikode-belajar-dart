import 'dart:io';

void main() {
  stdout.write("Jumlah perulangan: ");
  int n = int.parse(stdin.readLineSync());

  int i = 0;

  // do while
  do {
    print("Ini perulangan ke-${i + 1}");
    i++;
  } while (i < n);
}
