import 'dart:io';

void main() {
  stdout.write("Jumlah perulangan: ");
  int n = int.parse(stdin.readLineSync());

  int i = 0;

  // while
  while (i < n) {
    print("Ini perulangan ke-${i + 1}");
    i++;
  }
}
