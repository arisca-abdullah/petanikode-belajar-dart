import 'dart:io';

// Membuat fungsi
// return => mengembalikan nilai
String getLabel(String name, int v) {
  return "Sebuah kubus $name memiliki volume $v.";
}

int volumeKubus(int r) {
  return r * r * r;
}

void main() {
  stdout.write("Masukkan nama kubus: ");
  String nama = stdin.readLineSync();

  stdout.write("Masukkan panjang rusuk: ");
  int r = int.parse(stdin.readLineSync());

  int volume = volumeKubus(r); // nilai kembalian diisikan ke dalam variabel

  print(getLabel(nama, volume)); // nilai kembalian sebagai parameter
}
