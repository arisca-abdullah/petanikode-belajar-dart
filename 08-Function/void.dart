import 'dart:io';

// Membuat fungsi
// void => tidak mengembalikan nilai
void cetakLuasPersegi(int s) {
  print("Luas perseginya adalah ${s * s}");
}

void main() {
  stdout.write("Masukkan panjang sisi: ");
  int s = int.parse(stdin.readLineSync());

  // Memanggil fungsi
  cetakLuasPersegi(s);
}
