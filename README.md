# Belajar Dart di [Petanikode](https://www.petanikode.com)

Belajar Instan Bahasa Pemrograman Dart. Sumber [https://www.petanikode.com/belajar-dart](https://www.petanikode.com/belajar-dart).

## Topik

1. Hello, World!
2. Variabel
3. Input & Output
4. Operator
5. Percabangan
6. Perulangan
7. List
8. Function
9. OOP dasar
